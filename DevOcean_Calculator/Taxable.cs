﻿namespace DevOcean_Calculator
{
    /// <summary>
    /// A class that determines whether an income is subject to taxes or not.
    /// </summary>
    class Taxable
    {
        private const decimal nonTaxableIncome = 1000;
        public Taxable()
        {
            this.NonTaxableIncome = nonTaxableIncome;
        }

        public decimal NonTaxableIncome { get; set; }

        public bool IsItTaxable(decimal income)
        {
            if (income > nonTaxableIncome)
            {
                return true;
            }
            return false;
        }
    }
}
