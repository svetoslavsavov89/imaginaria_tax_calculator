﻿using System;

namespace DevOcean_Calculator
{
    /// <summary>
    /// A class that calculates the Income Tax and returns the amount rounded to even ("Bankers Rounding")
    /// </summary>
    class IncomeTax : Taxable
    {
        private const decimal incomeTax = 0.1M;
        private decimal taxableIncome;
        private decimal owedTax;
        public decimal CalculateIncomeTax(decimal income) 
        {
            if (base.IsItTaxable(income))
            {
                taxableIncome = income - base.NonTaxableIncome;
                owedTax = taxableIncome * incomeTax;
                return Math.Round(owedTax, 2, MidpointRounding.ToEven);
            }
            return 0;
        }
    }
}
