﻿using System;

namespace DevOcean_Calculator
{
    /// <summary>
    /// A class that calculates the Social Contribution Tax and returns the amount rounded to even ("Bankers Rounding")
    /// </summary>
    class SocialContribution : Taxable
    {
        private const decimal socialContributionTax = 0.15M;
        private const decimal maxTaxableIncome = 3000;
        private decimal taxableIncome;
        private decimal owedTax;
        public decimal CalculateSocialContribution(decimal income)
        {
            if (base.IsItTaxable(income))
            {
                if (income > 3000)
                {
                    taxableIncome = maxTaxableIncome - base.NonTaxableIncome;
                }
                else
                {
                    taxableIncome = income - base.NonTaxableIncome;
                }

                owedTax = taxableIncome * socialContributionTax;
                return Math.Round(owedTax, 2, MidpointRounding.ToEven);
            }
            return 0;
        }
    }
}
