﻿using System;

namespace DevOcean_Calculator
{
    /// <summary>
    /// A class that gives the user a choice to continue using the calculator or to quit.
    /// </summary>
    public class Continue
    {
        public bool IsContinue()
        {
            bool condition = false;
            bool answer = false;

            while (!condition)
            {
                Console.WriteLine("Do you want to continue? (Y/N)");

                string whantToContinue = Console.ReadLine();

                if (whantToContinue.ToLower() == "y")
                {
                    condition = true;
                    answer = true;

                }
                else if (whantToContinue.ToLower() == "n")
                {
                    condition = true;
                    answer = false;
                    Console.WriteLine("Goodbye!");
                }
                else
                {
                    Console.WriteLine("Please enter a valid answer!");
                    continue;
                }
            }
            return answer;
        }
    }
}
