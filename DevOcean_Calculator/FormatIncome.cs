﻿using System;

namespace DevOcean_Calculator
{
    /// <summary>
    /// A class that formats the user input to proper decimal expression and returns it if necessary rounded to even ("Bankers Rounding")
    /// </summary>
    class FormatIncome
    {
        decimal formatedIncome;
        public decimal Format(string income) 
        {
            if (income.Contains(','))
            {
                income = income.Replace(',', '.');
            }
            try
            {
                formatedIncome = decimal.Parse(income);
            }
            catch (Exception)
            {
                return -1;
            }

            if (formatedIncome > 0)
            {
                return Math.Round(formatedIncome, 2, MidpointRounding.ToEven);
            }
            else
            {
                return -1;
            }
        }
    }
}
