﻿using System;

namespace DevOcean_Calculator
{
    /// <summary>
    /// Console application that calculates the net salary by given gross value as input.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            SocialContribution socialContributionTax = new SocialContribution();
            IncomeTax incomeTax = new IncomeTax();
            Continue wantToContinue = new Continue();
            FormatIncome formatIncome = new FormatIncome();
            bool condition = true;

            Console.WriteLine("Welcome to the Imaginaria Tax Calculator!");

            while (condition)
            {
                Console.Write("Please enter your gross income here: ");

                string input = Console.ReadLine();

                decimal grossSalary = formatIncome.Format(input);

                if (grossSalary < 0)
                {
                    Console.WriteLine("Incorrect input!");
                    continue;
                }

                decimal owedIncomeTax = incomeTax.CalculateIncomeTax(grossSalary);

                decimal owedSocialContribution = socialContributionTax.CalculateSocialContribution(grossSalary);

                decimal totalTaxes = Math.Round((owedIncomeTax + owedSocialContribution), 2, MidpointRounding.ToEven);

                decimal netSalary = Math.Round((grossSalary - totalTaxes), 2, MidpointRounding.ToEven);

                Console.WriteLine($"Your gross income is {grossSalary} IDR");
                Console.WriteLine($"Your income tax is {owedIncomeTax} IDR");
                Console.WriteLine($"Your social contribution tax is {owedSocialContribution} IDR");
                Console.WriteLine($"Your net income is {netSalary} IDR");

                condition = wantToContinue.IsContinue();
            }
        }
    }
}
